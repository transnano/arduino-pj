const int dig1 = 1;
const int dig2 = 2;
const int dig3 = 3;
const int dig4 = 4;

const int a = 12;
const int b = 10;
const int c = 8;
const int d = 6;
const int e = 5;
const int f = 11;
const int g = 9;
const int dp = 7;

// 全てのDIG(桁)をクリア
void clearDigit()
{
  for (int i = 1; i <= 4; i++)
  {
    digitalWrite(i, LOW);
  }
}
// 全てのセグ(A-G,DP)をクリア
void clearSegment()
{
  for (int i = 5; i < 13; i++)
  {
    digitalWrite(i, HIGH);
  }
}

void setup()
{
  // 全てのピンを出力にする
  for (int i = 1; i <= 13; i++)
  {
    pinMode(i, OUTPUT);
  }

  // 全てのDIG(桁)をクリア
  clearDigit();

  // 全てのセグ(A-G,DP)をクリア
  clearSegment();
}

void loop()
{
  // DIG.1(桁数1) → 1を表示
  clearSegment();
  clearDigit();
  digitalWrite(dig1, HIGH);
  digitalWrite(b, LOW);
  digitalWrite(c, LOW);
  delayMicroseconds(50);
  Serial.println("Digit 1.");

  // DIG.2(桁数2) → 2を表示
  clearSegment();
  clearDigit();
  digitalWrite(dig2, HIGH);
  digitalWrite(a, LOW);
  digitalWrite(b, LOW);
  digitalWrite(d, LOW);
  digitalWrite(e, LOW);
  digitalWrite(g, LOW);
  delayMicroseconds(50);
  Serial.println("Digit 2.");

  // DIG.3(桁数3) → 3を表示
  clearSegment();
  clearDigit();
  digitalWrite(dig3, HIGH);
  digitalWrite(a, LOW);
  digitalWrite(b, LOW);
  digitalWrite(c, LOW);
  digitalWrite(d, LOW);
  digitalWrite(g, LOW);
  delayMicroseconds(50);
  Serial.println("Digit 3.");

  // DIG.4(桁数4) → 4を表示
  clearSegment();
  clearDigit();
  digitalWrite(dig4, HIGH);
  digitalWrite(b, LOW);
  digitalWrite(c, LOW);
  digitalWrite(f, LOW);
  digitalWrite(g, LOW);
  delayMicroseconds(50);
  Serial.println("Digit 4.");
}
