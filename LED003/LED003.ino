// 7セグメントLED x4の制御

int a = 1; //LED 11pin
int b = 2; //LED 7pin
int c = 3; //LED 4pin
int d = 4; //LED 2pin
int e = 5; //LED 1pin
int f = 6; //LED 10pin
int g = 7; //LED 5pin
int dp = 8; //LED3pin

//DIG選択用
int d1 = 12; //LED12pin
int d2 = 11; //LED 9pin
int d3 = 10; //LED 8pin
int d4 = 9; //LED 6pin

// すべて消す
void all_low(void)
{
  unsigned char j;
  for (j = 1; j <= 8; j++)
    digitalWrite(j, LOW);
}

//display number 1
void digital_1(void)
{
  all_low();
  digitalWrite(c, HIGH);
  digitalWrite(b, HIGH);
}
//display number2
void digital_2(void)
{
  all_low();
  digitalWrite(a, HIGH);
  digitalWrite(b, HIGH);
  digitalWrite(g, HIGH);
  digitalWrite(e, HIGH);
  digitalWrite(d, HIGH);
}
// display number3
void digital_3(void)
{
  all_low();
  digitalWrite(a, HIGH);
  digitalWrite(b, HIGH);
  digitalWrite(g, HIGH);
  digitalWrite(c, HIGH);
  digitalWrite(d, HIGH);
}
// display number4
void digital_4(void)
{
  all_low();
  digitalWrite(f, HIGH);
  digitalWrite(g, HIGH);
  digitalWrite(b, HIGH);
  digitalWrite(c, HIGH);
}
// display number5
void digital_5(void)
{
  all_low();
  digitalWrite(a, HIGH);
  digitalWrite(f, HIGH);
  digitalWrite(g, HIGH);
  digitalWrite(c, HIGH);
  digitalWrite(d, HIGH);
}
// display number6
void digital_6(void)
{
  all_low();
  digitalWrite(a, HIGH);
  digitalWrite(f, HIGH);
  digitalWrite(g, HIGH);
  digitalWrite(c, HIGH);
  digitalWrite(d, HIGH);
  digitalWrite(e, HIGH);
}
// display number7
void digital_7(void)
{
  all_low();
  digitalWrite(a, HIGH);
  digitalWrite(b, HIGH);
  digitalWrite(c, HIGH);
}
// display number8
void digital_8(void)
{
  all_low();
  digitalWrite(a, HIGH);
  digitalWrite(f, HIGH);
  digitalWrite(g, HIGH);
  digitalWrite(c, HIGH);
  digitalWrite(d, HIGH);
  digitalWrite(e, HIGH);
  digitalWrite(b, HIGH);
}
// display number9
void digital_9(void)
{
  all_low();
  digitalWrite(a, HIGH);
  digitalWrite(f, HIGH);
  digitalWrite(g, HIGH);
  digitalWrite(b, HIGH);
  digitalWrite(c, HIGH);
  digitalWrite(d, HIGH);
}
// display number0
void digital_0(void)
{
  all_low();
  digitalWrite(a, HIGH);
  digitalWrite(f, HIGH);
  digitalWrite(e, HIGH);
  digitalWrite(d, HIGH);
  digitalWrite(c, HIGH);
  digitalWrite(b, HIGH);
}
// display point
void dispDec(void)
{
  digitalWrite(dp, LOW);
}

void pickDigit(int x) {
  //DIGの選択
  digitalWrite(d1, HIGH);
  digitalWrite(d2, HIGH);
  digitalWrite(d3, HIGH);
  digitalWrite(d4, HIGH);
  delayMicroseconds(55);
  switch (x)
  {
    case 1:
      digitalWrite(d1, LOW); break;
    case 2:
      digitalWrite(d2, LOW); break;
    case 3:
      digitalWrite(d3, LOW); break;
    case 4:
      digitalWrite(d4, LOW); break;
    default:
      digitalWrite(d4, LOW); break;
  }
}

void pickNumber(int x) {
  switch (x)
  {
    case 1:
      digital_1(); //1
      break;
    case 2:
      digital_2();
      break;
    case 3:
      digital_3();
      break;
    case 4:
      digital_4();
      break;
    case 5:
      digital_5();
      break;
    case 6:
      digital_6();
      break;
    case 7:
      digital_7();
      break;
    case 8:
      digital_8();
      break;
    case 9:
      digital_9();
      break;
    case 0:
      digital_0();
      break;
    default:
      digital_0();
  }
}

long n = 0;
int x = 100;
int del = 155; //delay time

void setup()
{
  int i;//define i
  for (i = 1; i <= 12; i++)
    pinMode(i, OUTPUT); //set pin1~pin12 output
}
void loop()
{
  all_low();
  pickDigit(1);
  pickNumber(1);
  delayMicroseconds(del);

  all_low();
  pickDigit(2);
  pickNumber(2);
  delayMicroseconds(del);

  all_low();
  pickDigit(3);
  dispDec();
  pickNumber(3);
  delayMicroseconds(del);

  all_low();
  pickDigit(4);
  pickNumber(4);
  delayMicroseconds(del);
  n++;
}
