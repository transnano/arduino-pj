/* Stop-Watch Sketch
  https://ht-deko.com/arduino/uno_tutorial2.html#02_02_14
  Momentarily conectiong pin 13 to ground, either by
  push button or similar.starts and stops the count.
*/

// Pin declarations
// --segments
int a = 5;
int b = 7;
int c = 9;
int d = 11;
int e = 12;
int f = 6;
int g = 8;
int p = 10;

// --digits
int d4 = 4;
int d3 = 3;
int d2 = 2;
int d1 = 1;

//--start/stopreset control
int startStopReset = 13;

long n = 0;
int x = 100;
int del = 55;
int currState = 0;

void setup()
{
  pinMode(d1, OUTPUT);
  pinMode(d2, OUTPUT);
  pinMode(d3, OUTPUT);
  pinMode(d4, OUTPUT);
  pinMode(a, OUTPUT);
  pinMode(b, OUTPUT);
  pinMode(c, OUTPUT);
  pinMode(d, OUTPUT);
  pinMode(e, OUTPUT);
  pinMode(f, OUTPUT);
  pinMode(g, OUTPUT);
  pinMode(p, OUTPUT);
  pinMode(startStopReset, INPUT_PULLUP); // set pull-up resister on
}

void loop()
{
  int swButtonState = digitalRead(startStopReset);
  if (swButtonState == LOW)
  {
    currState++;
    while (digitalRead(startStopReset) == LOW)
    {
      dispAll();
    }
  }
  clearLEDs();
  pickDigit(1);
  pickNumber((n / x / 1000) % 10);
  delayMicroseconds(del);

  clearLEDs();
  pickDigit(2);
  pickNumber((n / x / 100) % 10);
  delayMicroseconds(del);

  clearLEDs();
  pickDigit(3);
  dispDec(3);
  pickNumber((n / x / 10) % 10);
  delayMicroseconds(del);

  clearLEDs();
  pickDigit(4);
  pickNumber(n / x % 10);
  delayMicroseconds(del);

  if ((currState % 3) == 0) // reset state
    n = 0;
  else if ((currState % 3) == 1) // start state
    n++;
  else
  {
  } // stop state
}

void pickDigit(int x)
{
  digitalWrite(d1, HIGH);
  digitalWrite(d2, HIGH);
  digitalWrite(d3, HIGH);
  digitalWrite(d4, HIGH);

  switch (x)
  {
  case 1:
    digitalWrite(d1, LOW);
    break;
  case 2:
    digitalWrite(d2, LOW);
    break;
  case 3:
    digitalWrite(d3, LOW);
    break;
  default:
    digitalWrite(d4, LOW);
    break;
  }
}

void pickNumber(int x)
{
  switch (x)
  {
  default:
    zero();
    break;
  case 1:
    one();
    break;
  case 2:
    two();
    break;
  case 3:
    three();
    break;
  case 4:
    four();
    break;
  case 5:
    five();
    break;
  case 6:
    six();
    break;
  case 7:
    seven();
    break;
  case 8:
    eight();
    break;
  case 9:
    nine();
    break;
  }
}

void dispAll()
{
  for (int i = 1; i <= 4; i++)
  {
    clearLEDs();
    pickDigit(i);
    dispDec(i);
    pickNumber(8);
    delayMicroseconds(del);
  }
}

void dispDec(int x)
{
  digitalWrite(p, HIGH);
}

void clearLEDs()
{
  digitalWrite(a, LOW);
  digitalWrite(b, LOW);
  digitalWrite(c, LOW);
  digitalWrite(d, LOW);
  digitalWrite(e, LOW);
  digitalWrite(f, LOW);
  digitalWrite(g, LOW);
  digitalWrite(p, LOW);
}

void zero()
{
  digitalWrite(a, HIGH);
  digitalWrite(b, HIGH);
  digitalWrite(c, HIGH);
  digitalWrite(d, HIGH);
  digitalWrite(e, HIGH);
  digitalWrite(f, HIGH);
  digitalWrite(g, LOW);
}

void one()
{
  digitalWrite(a, LOW);
  digitalWrite(b, HIGH);
  digitalWrite(c, HIGH);
  digitalWrite(d, LOW);
  digitalWrite(e, LOW);
  digitalWrite(f, LOW);
  digitalWrite(g, LOW);
}

void two()
{
  digitalWrite(a, HIGH);
  digitalWrite(b, HIGH);
  digitalWrite(c, LOW);
  digitalWrite(d, HIGH);
  digitalWrite(e, HIGH);
  digitalWrite(f, LOW);
  digitalWrite(g, HIGH);
}

void three()
{
  digitalWrite(a, HIGH);
  digitalWrite(b, HIGH);
  digitalWrite(c, HIGH);
  digitalWrite(d, HIGH);
  digitalWrite(e, LOW);
  digitalWrite(f, LOW);
  digitalWrite(g, HIGH);
}

void four()
{
  digitalWrite(a, LOW);
  digitalWrite(b, HIGH);
  digitalWrite(c, HIGH);
  digitalWrite(d, LOW);
  digitalWrite(e, LOW);
  digitalWrite(f, HIGH);
  digitalWrite(g, HIGH);
}

void five()
{
  digitalWrite(a, HIGH);
  digitalWrite(b, LOW);
  digitalWrite(c, HIGH);
  digitalWrite(d, HIGH);
  digitalWrite(e, LOW);
  digitalWrite(f, HIGH);
  digitalWrite(g, HIGH);
}

void six()
{
  digitalWrite(a, HIGH);
  digitalWrite(b, LOW);
  digitalWrite(c, HIGH);
  digitalWrite(d, HIGH);
  digitalWrite(e, HIGH);
  digitalWrite(f, HIGH);
  digitalWrite(g, HIGH);
}

void seven()
{
  digitalWrite(a, HIGH);
  digitalWrite(b, HIGH);
  digitalWrite(c, HIGH);
  digitalWrite(d, LOW);
  digitalWrite(e, LOW);
  digitalWrite(f, LOW);
  digitalWrite(g, LOW);
}

void eight()
{
  digitalWrite(a, HIGH);
  digitalWrite(b, HIGH);
  digitalWrite(c, HIGH);
  digitalWrite(d, HIGH);
  digitalWrite(e, HIGH);
  digitalWrite(f, HIGH);
  digitalWrite(g, HIGH);
}

void nine()
{
  digitalWrite(a, HIGH);
  digitalWrite(b, HIGH);
  digitalWrite(c, HIGH);
  digitalWrite(d, HIGH);
  digitalWrite(e, LOW);
  digitalWrite(f, HIGH);
  digitalWrite(g, HIGH);
}
