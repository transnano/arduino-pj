float Single_Point_DFT(int16_t inputArray[], int startingIndex, int endingIndex, float sampleFreq, float freqToSampleFor)
{
  float N = endingIndex - startingIndex + 1; //N=number of samples
  float k = freqToSampleFor * N / sampleFreq;
  float radiansPerSample = 2.0 * 3.14159 * k / N;

  float RealSum = 0;
  float ImaginarySum = 0;
  for (int n = 0; n <= endingIndex - startingIndex; n++)
  {
    float angle = n * radiansPerSample;
    float voltage = inputArray[startingIndex + n] * (5.0 / 1023.0);

    RealSum += voltage * cos(angle);
    ImaginarySum -= voltage * sin(angle);
  }

  return sqrt(RealSum * RealSum + ImaginarySum * ImaginarySum) / N;
}

const int analogInPin = A0;
uint32_t startTime_uS;
uint32_t endTime_uS;
uint32_t sleepStart_uS = 0;
const int waitState = 0;
const int pushState = 1;
const int payState = 2;
int currentState = waitState;
const int length = 128;
const float volume = 0.02;

int Matrix[][3] = {{1, 2, 3},
                   {4, 5, 6},
                   {7, 8, 9},
                   {-10, 0, -1}};

void setup()
{
  Serial.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);

  setupDisplay();
}

void loop()
{
  int16_t values[length];

  while (1)
  {
    startTime_uS = micros();
    for (uint8_t n = 0; n < length; n++)
    {
      values[n] = analogRead(analogInPin);
    }
    endTime_uS = micros();

    float sampleFreq = length * 1000000.0 / (endTime_uS - startTime_uS);
    float result1209 = Single_Point_DFT(values, 0, length - 1, sampleFreq, 1209);
    float result1336 = Single_Point_DFT(values, 0, length - 1, sampleFreq, 1336);
    float result1477 = Single_Point_DFT(values, 0, length - 1, sampleFreq, 1477);
    float result697 = Single_Point_DFT(values, 0, length - 1, sampleFreq, 697);
    float result770 = Single_Point_DFT(values, 0, length - 1, sampleFreq, 770);
    float result852 = Single_Point_DFT(values, 0, length - 1, sampleFreq, 852);
    float result941 = Single_Point_DFT(values, 0, length - 1, sampleFreq, 941);

    int maxX = calcMaxX(result1209, result1336, result1477);
    int maxY = calcMaxY(result697, result770, result852, result941);

    if (maxX >= 0 && maxY >= 0)
    {
      // 数字が押された
      if (Matrix[maxY][maxX] > -1)
      {
        int n = Matrix[maxY][maxX];
        disp4Digits(n);
        digitalWrite(LED_BUILTIN, LOW);
      }
      // * が押された（なぜかこれが通常時オンになる）
      else if (Matrix[maxY][maxX] < -1)
      {
        clearAll();
        digitalWrite(LED_BUILTIN, HIGH);
      }
      // # が押された（決済）
      else
      {
      }
    }
    // 来てない（その他）
    else
    {
      clearAll();
      digitalWrite(LED_BUILTIN, LOW);
    }
    Serial.print(maxX);
    Serial.print(", ");
    Serial.println(maxY);
  }
}

int calcMaxX(float val1, float val2, float val3)
{
  if (val1 > val2)
    if (val1 > val3)
      return val1 > volume ? 0 : -1;
    else
      return val3 > volume ? 2 : -1;
  else if (val2 > val3)
    return val2 > volume ? 1 : -1;
  else
    return val3 > volume ? 2 : -1;
}

int calcMaxY(float val1, float val2, float val3, float val4)
{
  if (val1 > val2)
    if (val1 > val3)
      if (val1 > val4)
        return val1 > volume ? 0 : -1;
      else
        return val4 > volume ? 3 : -1;
    else if (val3 > val4)
      return val3 > volume ? 2 : -1;
    else
      return val4 > volume ? 3 : -1;
  else if (val2 > val3)
    if (val2 > val4)
      return val2 > volume ? 1 : -1;
    else
      return val4 > volume ? 3 : -1;
  else if (val3 > val4)
    return val3 > volume ? 2 : -1;
  else
    return val4 > volume ? 3 : -1;
}
