const int led1 = 8;

void setup()
{
  Serial.begin(9600);
  pinMode(led1, OUTPUT);
}

void loop()
{
  double volume = analogRead(0);
  Serial.println(volume);
  // 有音の範囲
  if (volume > 400 || volume < 300)
  {
    digitalWrite(led1, HIGH);
  }
}
