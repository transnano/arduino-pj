/* Stop-Watch Sketch
  https://ht-deko.com/arduino/uno_tutorial2.html#02_02_14
  Momentarily conectiong pin 13 to ground, either by
  push button or similar.starts and stops the count.
*/

// Pin declarations
// --segments
int a = 5;
int b = 7;
int c = 9;
int d = 11;
int e = 12;
int f = 6;
int g = 8;
int p = 10;

// --digits
int d4 = 4;
int d3 = 3;
int d2 = 2;
int d1 = 1;

// --state
int initialState = 0;
int shuffleState = 1;
int resultState = 2;
int stateCount = 3;

//--start/stopreset control
int startStopReset = 13;

long n = 0;
int x = 100;
int del = 55;
int currState = initialState;
int randNumber; // random number for shuffle.

void setup()
{
  // Serial.begin(9600);
  randomSeed(analogRead(0)); // 未接続ピンのノイズを利用

  pinMode(d1, OUTPUT);
  pinMode(d2, OUTPUT);
  pinMode(d3, OUTPUT);
  pinMode(d4, OUTPUT);
  pinMode(a, OUTPUT);
  pinMode(b, OUTPUT);
  pinMode(c, OUTPUT);
  pinMode(d, OUTPUT);
  pinMode(e, OUTPUT);
  pinMode(f, OUTPUT);
  pinMode(g, OUTPUT);
  pinMode(p, OUTPUT);
  pinMode(startStopReset, INPUT_PULLUP); // set pull-up resister on
}

void loop()
{
  int swButtonState = digitalRead(startStopReset);

  // shuffle time
  if (swButtonState == LOW)
  {
    currState++;
    while (digitalRead(startStopReset) == LOW)
    {
      // randNumber = (int)random(9999);
      // disp4Digits(randNumber);
      // delayMicroseconds(2000 * 1000);
      for (int i = 1; i <= 4; i++)
      {
        clearLEDs();
        pickDigit(i);
        pickNumber((int)random(9));
        delayMicroseconds(del);
        delayMicroseconds(500 * 1000);
      }
    }
    randNumber = (int)random(9999);
  }
  // initial state
  if (currState == initialState)
  {
    dispAll();
  }
  // other
  else
    disp4Digits(randNumber);
}

void disp4Digits(int x)
{
  if (x > 9999)
  {
    x = 9999;
  }
  int y = x;
  clearLEDs();
  pickDigit(1);
  pickNumber(y / 1000);
  delayMicroseconds(del);

  y %= 1000;
  clearLEDs();
  pickDigit(2);
  pickNumber(y / 100);
  delayMicroseconds(del);

  y %= 100;
  clearLEDs();
  pickDigit(3);
  pickNumber(y / 10);
  delayMicroseconds(del);

  y %= 10;
  clearLEDs();
  pickDigit(4);
  pickNumber(y);
  delayMicroseconds(del);
}

void pickDigit(int x)
{
  digitalWrite(d1, HIGH);
  digitalWrite(d2, HIGH);
  digitalWrite(d3, HIGH);
  digitalWrite(d4, HIGH);

  switch (x)
  {
  case 1:
    digitalWrite(d1, LOW);
    break;
  case 2:
    digitalWrite(d2, LOW);
    break;
  case 3:
    digitalWrite(d3, LOW);
    break;
  default:
    digitalWrite(d4, LOW);
    break;
  }
}

void pickNumber(int x)
{
  switch (x)
  {
  default:
    zero();
    break;
  case 1:
    one();
    break;
  case 2:
    two();
    break;
  case 3:
    three();
    break;
  case 4:
    four();
    break;
  case 5:
    five();
    break;
  case 6:
    six();
    break;
  case 7:
    seven();
    break;
  case 8:
    eight();
    break;
  case 9:
    nine();
    break;
  }
}

void dispAll()
{
  for (int i = 1; i <= 4; i++)
  {
    clearLEDs();
    pickDigit(i);
    dispDec(i);
    pickNumber(8);
    delayMicroseconds(del);
  }
}

void dispDec(int x)
{
  digitalWrite(p, HIGH);
}

void clearLEDs()
{
  digitalWrite(a, LOW);
  digitalWrite(b, LOW);
  digitalWrite(c, LOW);
  digitalWrite(d, LOW);
  digitalWrite(e, LOW);
  digitalWrite(f, LOW);
  digitalWrite(g, LOW);
  digitalWrite(p, LOW);
}

void zero()
{
  digitalWrite(a, HIGH);
  digitalWrite(b, HIGH);
  digitalWrite(c, HIGH);
  digitalWrite(d, HIGH);
  digitalWrite(e, HIGH);
  digitalWrite(f, HIGH);
  digitalWrite(g, LOW);
}

void one()
{
  digitalWrite(a, LOW);
  digitalWrite(b, HIGH);
  digitalWrite(c, HIGH);
  digitalWrite(d, LOW);
  digitalWrite(e, LOW);
  digitalWrite(f, LOW);
  digitalWrite(g, LOW);
}

void two()
{
  digitalWrite(a, HIGH);
  digitalWrite(b, HIGH);
  digitalWrite(c, LOW);
  digitalWrite(d, HIGH);
  digitalWrite(e, HIGH);
  digitalWrite(f, LOW);
  digitalWrite(g, HIGH);
}

void three()
{
  digitalWrite(a, HIGH);
  digitalWrite(b, HIGH);
  digitalWrite(c, HIGH);
  digitalWrite(d, HIGH);
  digitalWrite(e, LOW);
  digitalWrite(f, LOW);
  digitalWrite(g, HIGH);
}

void four()
{
  digitalWrite(a, LOW);
  digitalWrite(b, HIGH);
  digitalWrite(c, HIGH);
  digitalWrite(d, LOW);
  digitalWrite(e, LOW);
  digitalWrite(f, HIGH);
  digitalWrite(g, HIGH);
}

void five()
{
  digitalWrite(a, HIGH);
  digitalWrite(b, LOW);
  digitalWrite(c, HIGH);
  digitalWrite(d, HIGH);
  digitalWrite(e, LOW);
  digitalWrite(f, HIGH);
  digitalWrite(g, HIGH);
}

void six()
{
  digitalWrite(a, HIGH);
  digitalWrite(b, LOW);
  digitalWrite(c, HIGH);
  digitalWrite(d, HIGH);
  digitalWrite(e, HIGH);
  digitalWrite(f, HIGH);
  digitalWrite(g, HIGH);
}

void seven()
{
  digitalWrite(a, HIGH);
  digitalWrite(b, HIGH);
  digitalWrite(c, HIGH);
  digitalWrite(d, LOW);
  digitalWrite(e, LOW);
  digitalWrite(f, LOW);
  digitalWrite(g, LOW);
}

void eight()
{
  digitalWrite(a, HIGH);
  digitalWrite(b, HIGH);
  digitalWrite(c, HIGH);
  digitalWrite(d, HIGH);
  digitalWrite(e, HIGH);
  digitalWrite(f, HIGH);
  digitalWrite(g, HIGH);
}

void nine()
{
  digitalWrite(a, HIGH);
  digitalWrite(b, HIGH);
  digitalWrite(c, HIGH);
  digitalWrite(d, HIGH);
  digitalWrite(e, LOW);
  digitalWrite(f, HIGH);
  digitalWrite(g, HIGH);
}
