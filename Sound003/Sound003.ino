/*
   Rui Santos
   Complete Project Details http://randomnerdtutorials.com
*/

int ledPin = 13;
int sensorPin = 7;
boolean val = 0;

void setup() {
  pinMode(ledPin, OUTPUT);
  pinMode(sensorPin, INPUT);
  Serial.begin (9600);
}

void loop() {
  long sum = 0;
  for (int i = 0; i < 100; i++)
  {
    sum += analogRead(sensorPin);
  }

  sum = sum / 100;

  if (sum > 70) {
    digitalWrite(ledPin, HIGH);
  } else {
    digitalWrite(ledPin, LOW);
  }

  Serial.println(sum);
  delay(10);
}
